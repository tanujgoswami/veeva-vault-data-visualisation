module Veeva
  module Vault
    class Study
      include HTTParty

      base_uri Rails.application.config.x.veeva.vault.domain

      HEADERS = {'Cache-Control' => 'no-cache',
                 'Content-Type' => 'application/json'}.freeze

      def initialize
        @auth_header = authorisation_header
        @version = Rails.application.config.x.veeva.vault.version
      end

      def all
        fetch_all("/api/#{@version}/vobjects/study__v", {})
      end

      def find(id)
        fetch("/api/#{@version}/vobjects/study__v/#{id}", {})
      end

      def site_legend
        legend(site_status_colours)
      end

      def country_legend
        legend(country_status_colours)
      end

      def countries(id)
        countries = []
        res = fetch_all("/api/#{@version}/query", { q: "SELECT id, name__v, (SELECT id, name__v FROM study_countries__vr) FROM study__v WHERE id = '#{id}'"})
        res.first['study_countries__vr']['data'].each do |country|
          c = fetch("/api/#{@version}/vobjects/study_country__v/#{country['id']}", {})
          geocode = geocode(c['name__v'])
          countries << Hash[id: geocode[:country_code],
                            name: c['name__v'],
                            status: c['status__v'].first.split('__').first.humanize,
                            fill: country_status_colours[c['status__v'].first.to_sym],
                            url: "#{Rails.application.config.x.veeva.vault.domain}/ui/#t/0TB000000000K15/0SC/#{country['id']}"]
        end
        countries
      end

      def sites(id)
        sites = []
        status_colours = site_status_colours
        res = fetch_all("/api/#{@version}/query", { q: "SELECT id, name__v, (SELECT id, site_name__v, site_status__v, principal_investigator__v FROM sites__vr) FROM study__v WHERE id = '#{id}'"})
        res.first['sites__vr']['data'].each do |site|
          locations = site_locations(site['id'])
          locations.each do |location|
            geocode = geocode("#{location['town_city__ctms']} #{location['state_province_region__clin']} #{location['postal_zip_code__ctms']}")
            sites << Hash[title: site['site_name__v'],
                          location: location['town_city__ctms'],
                          latitude: geocode[:lat],
                          longitude: geocode[:lon],
                          name: principal_investigator(site['principal_investigator__v']),
                          status: site['site_status__v'].first.split('__').first.humanize,
                          colour: status_colours[site['site_status__v'].first],
                          url: "#{Rails.application.config.x.veeva.vault.domain}/ui/#t/0TB000000000K17/0SI/#{site['id']}"]
          end
        end
        sites
      end

      private

      def legend(data)
        legend = []
        data.each do |status|
          legend << Hash[name: status.first.to_s.split('__').first.humanize, fill: status.second]
        end
        legend
      end

      def site_locations(site_id)
        res = fetch_all("/api/#{@version}/query", { q: "SELECT id, name__v, (SELECT id, name__v, town_city__ctms, state_province_region__clin, postal_zip_code__ctms FROM study_site_locations__ctmsr) FROM site__v WHERE id = '#{site_id}'"})
        res.first['study_site_locations__ctmsr']['data']
      end

      def principal_investigator(investigator_id)
        result = fetch("/api/#{@version}/vobjects/person__sys/#{investigator_id}", {})
        result['name__v']
      end

      def geocode(name)
        result = Geocoder.search(name)
        {country_code: result.first.data['address']['country_code'].try(&:upcase), lat: result.first.data['lat'].to_i, lon: result.first.data['lon'].to_i}
      end

      def site_status_colours
        colors = {}
        site_colors = fetch_all("/api/#{@version}/vobjects/study_site_colour__c", {})
        site_colors.each do |site_color|
          c = fetch("/api/#{@version}/vobjects/study_site_colour__c/#{site_color['id']}", {})
          colors[c['site_status__c'].first] = c['site_status_color__c'].downcase
        end
        colors
      end

      def country_status_colours
        {active__v: '#f5cc49', inactive__v: '#FEE8DE', in_migration__v: '#FEE8DE', archived__v: '#FEEDD8'}
      end

      def fetch_all(path, query)
        data = []
        loop do
          response = self.class.get(path, query: query, headers: HEADERS.merge(@auth_header)).parsed_response
          response['data'].each { |d| data << d }
          break if response['responseDetails']['next_page'].nil?
          path = response['responseDetails']['next_page']
        end
        data
      end

      def fetch(path, query)
        self.class.get(path, query: query, headers: HEADERS.merge(@auth_header)).parsed_response['data']
      end

      def authorisation_header
        params = {username: Rails.application.config.x.veeva.vault.username,
                  password: Rails.application.config.x.veeva.vault.password}
        session_id = self.class.post("/api/#{@version}/auth", query: params, headers: HEADERS).parsed_response['sessionId']
        { 'Authorization' => session_id }
      end
    end
  end
end
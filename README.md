# Veeva Vault Data Visualisation


# Build status

[![Build status](https://gitlab.com/tanujgoswami/veeva-vault-data-visualisation/badges/master/pipeline.svg)](https://gitlab.com/tanujgoswami/veeva-vault-data-visualisation/-/commits/master)


## Introduction

The Veeva Vault data visualisation site is used to chart Sites and countries involved in a study

The system ties in with a relatively number of external dependencies in order to integrate all the information about
a user. These connections are:

* Veeva Vault (via an API, see lib/veeva/vault/study.rb)
* OpenStreetMaps (via an API, see [Geocoder](https://github.com/alexreisner/geocoder))
* Armcharts (via JS, see [Armcharts](https://www.amcharts.com/docs/v4/))

## Configuration

Configuration of the environments is managed using environmental variables. On the production system the webserver is
passed the environmental variables which are downloaded to the running node by a service. On your development 
environment the "dotenv" gem is used which allows you to add environmental variable files:

Environmental variables for development go into:

    .env.development

The [Environmentor gem](https://github.com/madebymany/environmentor) is used to load the config from environmental variables. For a complete* list of configuration values please look at the 'config/external.rb' file. An example config can be seen in the '.env.example' file.

## Development

### Requirements

In order to get started developing you will need to ensure you have the following ready

* *nix environment
* Ruby - see '/.ruby-version' (RVM is recommended for managing versions and Gemsets).
* .env file from someone who else - mainly for Saleforce credentials

### Getting started
    
#### Ruby & Gems

Ensure you have the correct version of Ruby installed and have Bundler installed. The version of ruby must match the .ruby-version file.

If you are using RVM you can install Ruby by running the following in the project folder:

    $ rvm install .
    
Create a new "gemset" for the project - this encapsulates all the gems for the project.

    $ rvm gemset use --create veeva-vault-data-visualisation
    
Install Bundler:

    $ gem install bundler
    
Once bundler is installed you can install all the gem dependencies by running

    $ bundle install
    
####  Development config

You will need to create a .env.development file in your project root. It will need to contain a number of environmental 
variables in the format VARIABLE=value. Full listing can be found in .env.example. 

#### Starting the web server

To start the web server:

    $ rails s -b 0.0.0.0

## Deployment

Once code is committed into the master branch a Gitlab-CI pipeline is triggered to deploy the code in Heroku. (via .gitlab-ci.yml) 

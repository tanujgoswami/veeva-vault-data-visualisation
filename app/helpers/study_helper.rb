module StudyHelper
  STUDY = Veeva::Vault::Study.new

  def countries
    STUDY.countries(@study['id']).to_json.html_safe
  end

  def sites
    STUDY.sites(@study['id']).to_json.html_safe
  end

  def site_legend
    STUDY.site_legend.to_json.html_safe
  end

  def country_legend
    STUDY.country_legend.to_json.html_safe
  end
end
class StudiesController < ApplicationController
  def index
    @studies = study.all
  end

  def show
    @study = study.find(params[:id])
  end

  private

  def study
    Veeva::Vault::Study.new
  end
end

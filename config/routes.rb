Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'studies#index'

  resources :studies, only: %i[index show]
end

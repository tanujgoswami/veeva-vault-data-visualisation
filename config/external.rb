require 'securerandom'

Dotenv::Railtie.load if defined?(Dotenv)

module ExternalConfig
  extend Environmentor::Configurable

  environmentor.with_mapper :env do
    attr_config :secret_key_base, type: :string, required: Rails.env.production?

    namespace :veeva do
      namespace :vault do
        attr_config :username, type: :string, required: Rails.env.production?
        attr_config :password, type: :string, required: Rails.env.production?
        attr_config :domain, type: :string, required: Rails.env.production?
        attr_config :version, type: :string, required: Rails.env.production?
      end
    end
  end
end

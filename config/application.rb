require_relative 'boot'

require 'rails'
# Pick the frameworks you want:
require 'action_controller/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

require File.expand_path('../external', __FILE__)

module VeevaVaultDataVisualisation
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Don't generate system test files.
    config.generators.system_tests = nil

    # Delegate to environmentor config
    ::ExternalConfig.environmentor.delegate_from config.x.singleton_class

    config.eager_load_paths << Rails.root.join('lib')
  end
end
